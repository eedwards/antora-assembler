= Component Versions Key
:navtitle: Component Versions
:description: The component_versions key indicates the component versions from which the Antora PDF extension should generate PDFs.

The `component_versions` key indicates the component versions from which the Antora PDF extension should generate PDFs.

[#component-versions-key]
== component_versions key

The `component_versions` key is an optional key that can be set in [.path]_antora-assembler.yml_.
It accepts the version and component name of specific component versions, component names, or an array of picomatch patterns as values.

Here's an example that shows how to configure the PDF extension to generate a PDF (or PDFs) only for the last version of a component named `component-name`.

[,yaml]
----
component_versions: 'component-name'
----

The value can also be expressed as an array:

[,yaml]
----
component_versions: ['component-name']
----

Here are examples of the syntax you can use for the value, or each entry in an array value.
To avoid YAML syntax errors, it's best to enclose each value in single quotes.

`+*+`:: _Default_.
This value matches only the latest version of all components.
If the `component_versions` isn't specified, the extension will automatically use this value at runtime.
`+**+`:: This value matches all components and versions.
`component-name`:: The value of a component's name matches the latest version of the specified component.
`{component-name-1,component-name-2}`:: The value of two or more component names in a comma-separated list matches the latest version of each of the listed components.
`version@component-name`:: The value of a version and component name, `version@component-name`, matches the specified component version.
`{version@component-name,version@component-name}`:: The value of two or more component versions in a comma-separated list matches the specified component versions.
`+*@component-name+`:: The value of `+*@component-name+` matches all versions of the specified component.

Any value can be negated by prefixing it with `!`.
Once you select certain components and versions, you can then remove one or more of them from the list using negated entries.
For example, to run the PDF extension on the last version of every component except for `component-name`, use the following value:

[,yaml]
----
component_versions: ['*', '!component-name']
----

The array value can have any number of entries.

//== Generate PDFs for specific component versions

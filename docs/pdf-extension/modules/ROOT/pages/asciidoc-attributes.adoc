= AsciiDoc Attributes

Built-in and custom AsciiDoc document attributes can be applied to each compiled AsciiDoc document when the PDF extension is generating your PDFs.

== Assign attributes in antora-assembler.yml

AsciiDoc document attributes set in [.path]_antora-assembler.yml_ supplement attributes defined in the playbook, component version descriptors, and pages.
See xref:antora:playbook:asciidoc-attributes.adoc[] for precedence rules.
Attributes are set and assigned in [.path]_antora-assembler.yml_ just as they are in the playbook or component version descriptors.
Attributes are mapped under the `asciidoc` and `attributes` keys.

.antora-assembler.yml
[,yaml]
----
component_versions: '{colorado,1.5.6@colorado}'
asciidoc:
  attributes:
    secnums: ''
    xrefstyle: full
    outlinelevels: '2'
----

Attributes assigned in [.path]_antora-assembler.yml_ are applied to all the PDFs according to the xref:antora:playbook:asciidoc-attributes.adoc#precedence-rules[attribute precedence rules].

== Generate PDFs with TOCs

To add a Table of Contents to each generated PDF, set the `toc` attribute in [.path]_antora-assembler.yml_.

.antora-assembler.yml
[,yaml]
----
component_versions: '{colorado,1.5.6@colorado}'
asciidoc:
  attributes:
    toc: ''
----

== Activate a custom PDF theme

The `pdf-theme` attribute is used to activate and apply a custom theme to your PDFs.
The `pdf-theme` attribute accepts the name of a YAML file stored in your playbook directory.
In this example, the file is named [.path]_pdf-theme.yml_.

.antora-assembler.yml
[,yaml]
----
component_versions: '{colorado,1.5.6@colorado}'
asciidoc:
  attributes:
    pdf-theme: ./pdf-theme.yml
----

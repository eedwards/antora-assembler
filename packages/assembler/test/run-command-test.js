/* eslint-env mocha */
'use strict'

const { expect } = require('@antora/assembler-test-harness')
const fsp = require('node:fs/promises')
const { runCommand } = require('@antora/assembler')
const node = process.execPath
const ospath = require('node:path')

const FIXTURES_DIR = ospath.join(__dirname, 'fixtures', ospath.basename(__filename, '.js'))

// escape arguments (Windows only)

describe('runCommand()', () => {
  // TODO move to test harness; maybe a more fitting name?
  const deferError = (fn) =>
    fn().then(
      (returnValue) => () => returnValue,
      (err) => () => {
        throw err
      }
    )

  const captureStdStream = async function (stream, fn) {
    const streamWrite = stream.write
    try {
      const data = []
      stream.write = (buffer) => data.push(buffer)
      await fn()
      return data.join('')
    } finally {
      stream.write = streamWrite
    }
  }

  const captureStdout = captureStdStream.bind(null, process.stdout)

  const captureStderr = captureStdStream.bind(null, process.stderr)

  it('should throw TypeError if command is not specified', async () => {
    expect(await deferError(runCommand)).to.throw(TypeError, 'Command not specified')
  })

  it('should throw Error if command is not found', async () => {
    expect(await deferError(() => runCommand('no-such-command'))).to.throw(Error, 'Command not found: no-such-command')
  })

  it('should throw error if exit code of command is non-zero', async () => {
    expect(await deferError(() => runCommand('npm'))).to.throw(Error, 'Command failed: npm')
  })

  it('should return output of command with no arguments as Buffer', async () => {
    const whoami = require('node:child_process').execSync('whoami').toString()
    const actual = await runCommand('whoami')
    expect(actual).to.be.instanceOf(Buffer)
    expect(actual.toString()).to.equal(whoami)
  })

  it('should return output of command with arguments as Buffer', async () => {
    const actual = await runCommand('node', ['-p', '1'])
    expect(actual).to.be.instanceOf(Buffer)
    expect(actual.toString()).to.equal('1\n')
  })

  it('should throw error if exit code of command with arguments is non-zero', async () => {
    expect(await deferError(() => runCommand('npm', ['unknown']))).to.throw(Error, 'Command failed: npm unknown')
  })

  it('should include reason in Error message if command fails', async () => {
    const expected = `Command failed: ${node} --invalid\n${node}: bad option: --invalid`
    expect(await deferError(() => runCommand(node, ['--invalid']))).to.throw(Error, expected)
  })

  it('should split command on spaces and move subcommands to arguments', async () => {
    const actual = await runCommand('npm config get node-version')
    expect(actual).to.be.instanceOf(Buffer)
    expect(actual.toString()).to.equal(`${process.version}\n`)
  })

  it('should pass Buffer value of input option to stdin', async () => {
    const script = ospath.join(FIXTURES_DIR, 'cat-stdin.js')
    const input = Buffer.from('start\nstop')
    const actual = await runCommand(node, [script], { implicitStdin: true, input })
    expect(actual).to.be.instanceOf(Buffer)
    expect(actual.toString()).to.equal(input.toString())
  })

  it('should not throw EPIPE error if command cannot receive input because script is missing', async () => {
    const script = ospath.join(FIXTURES_DIR, 'no-such-script.js')
    const input = Buffer.from('filler\n'.repeat(1000000))
    const expected = `Command failed: ${node} ${script}\n`
    expect(await deferError(() => runCommand(node, [script], { implicitStdin: true, input }))).to.throw(Error, expected)
  })

  it('should not throw EPIPE error if command cannot receive input because option is invalid', async () => {
    const input = Buffer.from('filler\n'.repeat(1000000))
    const expected = `Command failed: ${node} --bogus\n${node}: bad option: --bogus`
    expect(await deferError(() => runCommand(node, ['--bogus'], { implicitStdin: true, input }))).to.throw(
      Error,
      expected
    )
  })

  it('should append - to argv by default if input is specified', async () => {
    const script = ospath.join(FIXTURES_DIR, 'spawn-args.js')
    const input = Buffer.from('start\nstop')
    const actual = await runCommand(node, [script], { input })
    expect(actual).to.be.instanceOf(Buffer)
    expect(actual.toString()).to.equal([node, script, '-'].join(' '))
  })

  it('should not append - to argv if input is specified and implicitStdin option is set to true', async () => {
    const script = ospath.join(FIXTURES_DIR, 'spawn-args.js')
    const input = Buffer.from('start\nstop')
    const actual = await runCommand(node, [script], { input, implicitStdin: true })
    expect(actual).to.be.instanceOf(Buffer)
    expect(actual.toString()).to.equal([node, script].join(' '))
  })

  it('should return lazy readable for output if specified', async () => {
    const script = ospath.join(FIXTURES_DIR, 'to-file.js')
    const lowerAlpha = Array(26)
      .fill(0)
      .map((_, idx) => String.fromCharCode(97 + idx))
    const input = Buffer.from(lowerAlpha.join('\n'))
    const output = ospath.join(FIXTURES_DIR, 'to-file.js-out.txt')
    try {
      const actual = await runCommand(node, [script, output], { implicitStdin: true, input, output })
      expect(actual.constructor.name).to.equal('LazyReadable')
      const chunks = []
      for await (const chunk of actual) chunks.push(chunk)
      expect(chunks.join('')).to.equal(input.toString())
    } finally {
      await fsp.unlink(output)
    }
  })

  it('should pass through stdout if output is specified', async () => {
    const script = ospath.join(FIXTURES_DIR, 'cat-stdin.js')
    const input = Buffer.from('hi')
    const output = script
    let actual
    const stdout = await captureStdout(async () => {
      actual = await runCommand(node, [script], { implicitStdin: true, input, output })
    })
    expect(actual.constructor.name).to.equal('LazyReadable')
    expect(stdout).to.equal('hi')
  })

  it('should pass through stderr if command does not fail', async () => {
    const script = ospath.join(FIXTURES_DIR, 'warn.js')
    const stderr = await captureStderr(() => runCommand(node, [script]))
    expect(stderr).to.equal('no no\n')
  })

  it('should rethrow error if reading input Buffer throws error', async () => {
    const script = ospath.join(FIXTURES_DIR, 'cat-stdin.js')
    const input = new Proxy(Buffer.alloc(0), {
      get () {
        throw new Error('stop')
      },
    })
    expect(await deferError(() => runCommand(node, [script], { input, implicitStdin: true }))).to.throw(Error, 'stop')
  })
})

'use strict'

const { compile: bracesToGroup } = require('braces')
const { makeRe: makePicomatchRx } = require('picomatch')

const PICOMATCH_OPTS = {
  bash: true,
  expandRange: (begin, end, step, opts) => bracesToGroup(opts ? `{${begin}..${end}..${step}}` : `{${begin}..${end}}`),
  fastpaths: false,
  nobracket: true,
  noglobstar: true,
  nonegate: true,
  noquantifiers: true,
  regex: false,
  strictSlashes: true,
}

const VERSION_SEPARATOR_RX = /@(?!\()/

function compilePatterns (patterns) {
  if (patterns[0].charAt() === '!') patterns = ['**', ...patterns]
  return patterns.map((pattern) => {
    const negated = pattern.charAt() === '!'
    if (negated) pattern = pattern.slice(1)
    let version
    const separatorIdx = pattern.search(VERSION_SEPARATOR_RX)
    if (~separatorIdx) {
      pattern = (version = true) && `${pattern.slice(0, separatorIdx)}%${pattern.slice(separatorIdx + 1) || '*'}`
    }
    return Object.assign(makePicomatchRx(pattern, PICOMATCH_OPTS), {
      globstar: pattern === '**',
      negated,
      star: pattern === '*',
      version,
    })
  })
}

function filterComponentVersions (components, patterns) {
  if (!patterns.length) return []
  const rxs = compilePatterns(patterns)
  return components.reduce((accum, { latest, versions }) => {
    accum.push(
      ...versions.filter((version) => {
        let matched
        for (const rx of rxs) {
          let voteIfMatched
          if (matched) {
            if (rx.negated) voteIfMatched = false
          } else if (!rx.negated) {
            voteIfMatched = true
          }
          if (voteIfMatched == null) continue
          if (rx.globstar) {
            matched = voteIfMatched
          } else if (rx.star) {
            if (version === latest) matched = voteIfMatched
          } else if (rx.version) {
            if (rx.test(`${version.version}%${version.name}`)) matched = voteIfMatched
          } else if (version === latest && rx.test(version.name)) {
            matched = voteIfMatched
          }
        }
        return matched
      })
    )
    return accum
  }, [])
}

module.exports = filterComponentVersions

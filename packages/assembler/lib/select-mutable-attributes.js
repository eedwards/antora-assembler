'use strict'

function selectMutableAttributes (loadAsciiDoc, contentCatalog, referencePage, asciidocConfig) {
  const doc = loadAsciiDoc(
    new referencePage.constructor(
      Object.assign({}, referencePage, { contents: Buffer.alloc(0), mediaType: 'text/asciidoc' })
    ),
    contentCatalog,
    asciidocConfig
  )
  const additionalMutableNames = [
    'page-component-name',
    'page-component-version',
    'page-version',
    'page-component-display-version',
    'page-component-title',
  ]
  // we could consider using an Asciidoctor extension here to grab attributes passed via the API instead
  const immutableAttributeNames = doc.attribute_overrides.$keys()['$-'](additionalMutableNames)
  return Object.entries(doc.getAttributes()).reduce((accum, [name, val]) => {
    if (!immutableAttributeNames.includes(name)) accum[name] = val
    return accum
  }, {})
}

module.exports = selectMutableAttributes

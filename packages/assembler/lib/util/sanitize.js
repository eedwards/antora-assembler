'use strict'

const XML_TAG_RX = /<[^>]+>/g
const XML_SPECIAL_CHARS = { '&lt;': '<', '&gt;': '>', '&amp;': '&' }
const XML_SPECIAL_CHARS_RX = /&(?:[lg]t|amp);/g

function sanitize (str) {
  return ~str.indexOf('<')
    ? [str.replace(XML_TAG_RX, '').replace(XML_SPECIAL_CHARS_RX, (m) => XML_SPECIAL_CHARS[m]), `+++${str}+++`, true]
    : ~str.indexOf('&')
        ? Array(2).fill(str.replace(XML_SPECIAL_CHARS_RX, (m) => XML_SPECIAL_CHARS[m]))
        : [str, str]
}

module.exports = sanitize

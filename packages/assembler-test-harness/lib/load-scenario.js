'use strict'

const { loadAsciiDoc, resolveAsciiDocConfig } = require('@antora/asciidoc-loader')
const loadAssemblerConfig = require('@antora/assembler/load-config')
const fsp = require('node:fs/promises')
const yaml = require('js-yaml')
const ospath = require('node:path')
const { posix: path } = ospath
const createFile = require('./create-file')

const $components = Symbol('components')
const $files = Symbol('files')

class ContentCatalog {
  constructor (components, files) {
    this[$components] = components
    this[$files] = files
  }

  getComponent (name) {
    return this[$components].find((it) => it.name === name)
  }

  getComponents () {
    return [...this[$components]]
  }

  getById ({ component, version, module: module_, family, relative }) {
    return this[$files].find(({ src }) => {
      return (
        src.component === component &&
        src.version === version &&
        src.module === module_ &&
        src.family === family &&
        src.relative === relative
      )
    })
  }

  getByPath ({ component, version, path: path_ }) {
    return this[$files].find(({ src }) => {
      return src.component === component && src.version === version && src.path === path_
    })
  }

  getComponentVersionStartPage (component, version) {
    return (
      this.getById({ component, version, module: 'ROOT', family: 'page', relative: 'index.adoc' }) ||
      (this.getById({ component, version, module: 'ROOT', family: 'alias', relative: 'index.adoc' }) || {}).rel
    )
  }

  resolvePage (spec, { component, version, module: module_ = 'ROOT' }) {
    let relative = spec
    if (~relative.indexOf(':')) {
      ;[module_, relative] = relative.split(':')
    }
    return (
      this.getById({ component, version, module: module_, family: 'page', relative }) ||
      (this.getById({ component, version, module: module_, family: 'alias', relative }) || {}).rel
    )
  }

  resolveResource (spec, { component, version, module: module_ = 'ROOT' }, defaultFamily = undefined) {
    let relative = spec
    if (~relative.indexOf(':')) {
      ;[module_, relative] = relative.split(':')
    }
    const family = defaultFamily || 'page'
    return this.getById({ component, version, module: module_, family, relative })
  }

  getPages () {
    return this[$files].filter(({ src }) => src.family === 'page')
  }
}

async function loadScenario (name, dirname) {
  const dir = ospath.join(dirname, 'scenarios', name)
  let data = await fsp.readFile(ospath.join(dir, 'data.yml')).then((contents) => yaml.load(contents))
  if (!data.version) data.version = ''
  data = Object.assign(
    {
      displayVersion: data.version || 'default',
      title: data.name,
      origin: {
        url: `https://github.com/acme/${data.name}`,
        startPath: '',
        branch: data.version ? `v${data.version}` : 'main',
        refhash: 'a00000000000000000000000000000000000000z',
      },
    },
    data
  )
  const asciidocConfig = resolveAsciiDocConfig()
  // TODO should use collateAsciiDocAttributes here to handle overrides properly
  Object.assign(asciidocConfig.attributes, data.asciidoc?.attributes)
  const componentVersion = {
    name: data.name,
    version: data.version,
    displayVersion: data.displayVersion,
    title: data.title,
    url: '/' + [data.name === 'ROOT' ? '' : data.name, data.version, 'index.html'].filter((it) => it).join('/'),
    asciidoc: asciidocConfig,
  }
  const component = { name: componentVersion.name, versions: [componentVersion], latest: componentVersion }
  const files = data.files.map((src) => {
    let asciidoc = src.relative.endsWith('.adoc')
    src = Object.assign(
      {
        component: componentVersion.name,
        version: componentVersion.version,
        module: 'ROOT',
        family: asciidoc ? 'page' : undefined,
      },
      src
    )
    src.path = path.join('modules', src.module, `${src.family}s`, src.relative) // required by getByPath
    const contents = src.contents || ''
    src.contents = Buffer.from(contents)
    src.origin = data.origin
    ;({ base: src.basename, ext: src.extname } = path.parse(src.relative))
    if (asciidoc) {
      // should we do this more formally?
      if (contents?.startsWith('= ')) {
        let doctitle = contents.split('\n')[0].slice(2)
        if (/[\x60_*#[]/.test(doctitle)) {
          const doctitleFile = { contents: doctitle, src: { relative: 'dummy.adoc' }, pub: { moduleRootPath: '' } }
          doctitle = loadAsciiDoc(doctitleFile, undefined, { doctype: 'inline' }).convert()
        }
        const navtitleIdx = contents.indexOf('\n:navtitle: ')
        const navtitle = ~navtitleIdx ? contents.slice(navtitleIdx + 12).split('\n')[0] : doctitle
        asciidoc = { doctitle, navtitle }
      } else {
        asciidoc = { doctitle: 'Untitled', navtitle: 'Untitled' }
      }
    }
    return Object.assign(createFile(src), { asciidoc })
  })
  const startPageSpec = data.startPage
  if (startPageSpec) {
    const startPage = files.find(({ src }) => src.relative === startPageSpec)
    if (startPage && !(startPage.src.relative === 'index.adoc' && startPage.src.module === 'ROOT')) {
      files.push(createFile(Object.assign({}, startPage.src, { family: 'alias', relative: 'index.adoc' }), startPage))
    }
  }
  const contentCatalog = new ContentCatalog([component], files)
  const assemblerConfig = await loadAssemblerConfig({ dir }, data.assembler)
  // FIXME support multiple navigation menus
  const navigationMenu = expandNavigation(data.navigation, contentCatalog, componentVersion)
  if (!navigationMenu.content) {
    if ('content' in navigationMenu) {
      delete navigationMenu.content
    } else {
      navigationMenu.content = componentVersion.title // convenience
    }
  }
  componentVersion.navigation = [navigationMenu]
  return { loadAsciiDoc, dir, componentVersion, contentCatalog, assemblerConfig }
}

function expandNavigation (item, contentCatalog, componentVersion) {
  const { content, items } = item
  if (content?.startsWith('xref:')) {
    const [, target, text] = /^xref:([^[]+)\[(.*?)\]$/.exec(content)
    const page = contentCatalog.getById({
      component: componentVersion.name,
      version: componentVersion.version,
      module: ~target.indexOf(':') ? target.split(':')[0] : 'ROOT',
      family: 'page',
      relative: ~target.indexOf(':') ? target.split(':')[1] : target,
    })
    if (page) {
      item.url = page.pub.url
      item.urlType = 'internal'
      item.content = text || page.asciidoc.navtitle
    }
  }
  if (items) {
    for (const item of items) expandNavigation(item, contentCatalog, componentVersion)
  }
  return item
}

module.exports = loadScenario
